<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FastFood Login</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <link rel="stylesheet" href="css/login.css">

</head>
<style>
    body {
        font-family: Arial, sans-serif;
            background-image: url('img/imagelogin.jpg');
            background-size: 100%;
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position: center;
    }

    .container {
        max-width: 400px;
        margin: 50px auto;
        background: #fff;
        padding: 20px;
        border-radius: 5px;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
    }

    .form-title {
        text-align: center;
    }

    .form-group {
        margin-bottom: 20px;
    }

    label {
        display: block;
        margin-bottom: 5px;
    }

    input.input-field {
        width: 100%;
        padding: 10px;
        border: 1px solid #ccc;
        border-radius: 3px;
    }

    .form-submit {
        width: 100%;
        padding: 10px;
        background-color: #007bff;
        color: #fff;
        border: none;
        border-radius: 3px;
        cursor: pointer;
    }

    .form-submit:hover {
        background-color: #0056b3;
    }

    .btn-link {
        color: #007bff;
        text-decoration: none;
        transition: color 0.3s; /* Hi?u ?ng chuy?n ??i m�u trong 0.3s */
    }

    .btn-link:hover {
        text-decoration: underline;
        color: #0056b3; /* M�u s?c khi di chu?t qua */
    }

    .error-message {
        color: red;
        margin-top: 10px;
    }
</style>
</head>
<body>

<div class="container">
    <h2 class="form-title">Login</h2>
    <p class="error-message">${mess}</p>

    <form action="login" method="get" class="register-form" id="login-form">

        <div class="form-group">
            <label for="username">
                <i class="zmdi zmdi-account material-icons-name"></i>
            </label> 
            <input required type="text" name="username" id="username" placeholder="Username" class="input-field">
        </div>

        <div class="form-group">
            <label for="password">
                <i class="zmdi zmdi-lock"></i>
            </label> 
            <input type="password" name="password" id="password" placeholder="Password" required class="input-field">
        </div>

        <div class="form-group form-button">
            <button type="submit" class="form-submit">Login</button>
        </div>

        <div class="text-center mt-3">
            <a href="register.jsp" class="btn-link" >Create an Account</a>
        </div>
        <div class="text-center mt-3">
            <a href="content" class="btn-link">Back to Home</a>
        </div>
    </form>
</div>

</body>

<!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
