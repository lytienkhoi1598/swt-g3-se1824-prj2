<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
    <title>Danh sách sản phẩm</title>
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }
        th {
            background-color: #f2f2f2;
        }
        tr:hover {
            background-color: #f5f5f5;
        }
    </style>
</head>
<body>

<h2>Danh sách sản phẩm</h2>

<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Tên</th>
            <th>Hình ảnh</th>
            <th>Giá</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach items="${listP}" var="product">
            <tr>
                <td>${product.Id}</td>
                <td>${product.Name}</td>
                <td><img src="${product.image}" alt="Hình ảnh sản phẩm" style="max-width: 100px;"></td>
                <td>${product.price} VND</td>
            </tr>
        </c:forEach>
    </tbody>
</table>

</body>
</html>
