<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>FastFood</title>

        <!-- Font Icon -->
        <link rel="stylesheet"
              href="fonts/material-icon/css/material-design-iconic-font.min.css">

        <!-- Main css -->
        <link rel="stylesheet" href="css/login.css">

    </head>
  <style>
     body {
        font-family: Arial, sans-serif;
            background-image: url('img/imagelogin.jpg');
            background-size: 100%;
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position: center;
    }


    .container {
        max-width: 400px;
        margin: 50px auto;
        background: #fff;
        padding: 20px;
        border-radius: 5px;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
    }

    .form-title {
        text-align: center;
    }

    .form-group {
        margin-bottom: 20px;
    }

    label {
        display: block;
        margin-bottom: 5px;
    }

    input.input-field {
        width: 100%;
        padding: 10px;
        border: 1px solid #ccc;
        border-radius: 3px;
    }

    .form-submit {
        width: 100%;
        padding: 10px;
        background-color: #007bff;
        color: #fff;
        border: none;
        border-radius: 3px;
        cursor: pointer;
    }

    .form-submit:hover {
        background-color: #0056b3;
    }

    .btn-link {
        color: #007bff;
        text-decoration: none;
    }

    .btn-link:hover {
        text-decoration: underline;
    }

    .error-message {
        color: red;
        margin-top: 10px;
        text-align: center;
    }
</style>
</head>
<body>

<div class="container">
    <h2 class="form-title">Register</h2>

    <p class="error-message">${mess}</p>

    <form action="register" method="get" class="register-form" id="register-form">

        <div class="form-group">
            <label for="username">
                <i class="zmdi zmdi-account material-icons-name"></i>
            </label> 
            <input required type="text" name="username" id="username" placeholder="Username" class="input-field">
        </div>

        <div class="form-group">
            <label for="password">
                <i class="zmdi zmdi-lock"></i>
            </label> 
            <input type="password" name="password" id="password" placeholder="Password" required class="input-field">
        </div>

        <div class="form-group">
            <label for="cfpassword">
                <i class="zmdi zmdi-lock"></i>
            </label> 
            <input type="password" name="cfpassword" id="cfpassword" placeholder="Repeat your password" required class="input-field">
        </div>

        <div class="form-group form-button">
            <button type="submit" class="form-submit">Submit</button>
        </div>

        <div class="text-center mt-3">
            <a href="login.jsp" class="btn btn-link">Back to Login</a>
        </div>
        <div class="text-center mt-3">
            <a href="content" class="btn btn-link">Back to Home</a>
        </div>
    </form>
</div>

</body>
    <!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>