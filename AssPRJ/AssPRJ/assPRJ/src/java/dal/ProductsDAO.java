package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.*;

public class ProductsDAO extends DBContext {

    //lay ra danh sách tat ca san pham
    public List<Products> getAllProducts() {
        List<Products> productList = new ArrayList<>();
        String sql = "SELECT * FROM Products";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                Products s = new Products(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("description"),
                        rs.getInt("price"),
                        rs.getString("image"),
                        rs.getInt("categoryID")
                );
                productList.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return productList;

    }

    public List<Products> getAllProductsByRandom() {
        List<Products> productList = new ArrayList<>();
        String sql = "SELECT *\n"
                + "FROM dbo.Products\n"
                + "ORDER BY NEWID();";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                Products s = new Products(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("description"),
                        rs.getInt("price"),
                        rs.getString("image"),
                        rs.getInt("categoryID")
                );
                productList.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return productList;

    }

    public List<Products> getNewProducts() {
        List<Products> productList = new ArrayList<>();
        String sql = "SELECT TOP (4) * FROM Products ORDER BY ID DESC";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                Products s = new Products(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("description"),
                        rs.getInt("price"),
                        rs.getString("image"),
                        rs.getInt("categoryID")
                );
                productList.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return productList;

    }

    //lay ra danh sách 4 sp lien quan
    public List<Products> getRandomProducts() {
        List<Products> productList = new ArrayList<>();
        String sql = "SELECT TOP(4) * FROM Products ORDER BY NEWID() ";
        try {
            int id, price, categoryID;
            String name, description, image;
            Products product;
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                id = rs.getInt("ID");
                name = rs.getString("Name");
                description = rs.getString("Description");
                price = rs.getInt("Price");
                image = rs.getString("Image");
                categoryID = rs.getInt("CategoryID");
                Products p = new Products(id, name, description, price, image, categoryID);
                productList.add(p);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return productList;
    }

    //lay ra san pham lien quan
    public List<Products> getRandomProducts(int cateID) {
        List<Products> productList = new ArrayList<>();
        String sql = "SELECT TOP(4) * FROM Products WHERE CategoryID = ? ORDER BY NEWID() ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, cateID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                Products s = new Products(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("description"),
                        rs.getInt("price"),
                        rs.getString("image"),
                        rs.getInt("categoryID")
                );
                productList.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return productList;
    }

    //lay ra 4 san pham bestseller
    public List<Products> getBestSellingProducts() {
        List<Products> productList = new ArrayList<>();
        String sql = "SELECT TOP 4 \n"
                + "    p.*,\n"
                + "    SUM(oi.Quantity) AS TotalQuantitySold\n"
                + "FROM \n"
                + "    Products p\n"
                + "JOIN \n"
                + "    OrderItems oi ON p.ID = oi.ProductID\n"
                + "GROUP BY \n"
                + "    p.ID, p.Name, p.Description, p.Price, p.Image, p.CategoryID\n"
                + "ORDER BY \n"
                + "    SUM(oi.Quantity) DESC;";
        try {
            int id, price, categoryID;
            String name, description, image;
            Products product;
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                id = rs.getInt("ID");
                name = rs.getString("Name");
                description = rs.getString("Description");
                price = rs.getInt("Price");
                image = rs.getString("Image");
                categoryID = rs.getInt("CategoryID");
                Products p = new Products(id, name, description, price, image, categoryID);
                productList.add(p);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return productList;
    }

    // lay ra san pham theo category
    public List<Products> getProductsByCategoryID(int categoryID) {
        List<Products> productList = new ArrayList<>();
        String sql = "SELECT * FROM Products WHERE CategoryID = ?";
        try {
            int id, price;
            String name, description, image;
            Products product;
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, categoryID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                id = rs.getInt("ID");
                name = rs.getString("Name");
                description = rs.getString("Description");
                price = rs.getInt("Price");
                image = rs.getString("Image");
                product = new Products(id, name, description, price, image, categoryID);
                productList.add(product);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return productList;
    }

    // lay san pham bang id
    public Products getProductByID(int productID) {
        String sql = "SELECT * FROM Products WHERE ID = ?";
        try {

            int id, price, categoryID;
            String name, description, image;
            Products product;
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, productID);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                id = rs.getInt("ID");
                name = rs.getString("Name");
                description = rs.getString("Description");
                price = rs.getInt("Price");
                image = rs.getString("Image");
                categoryID = rs.getInt("CategoryID");
                product = new Products(id, name, description, price, image, categoryID);
                return product;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    // hàm search san pham
    public List<Products> searchProductByName(String text) {
        List<Products> productList = new ArrayList<>();
        String sql = "SELECT * FROM Products WHERE Name LIKE ?";
        try {
            int id, price, sellerID, categoryID;
            String name, description, image;
            Products product;
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + text + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                id = rs.getInt("ID");
                name = rs.getString("Name");
                description = rs.getString("Description");
                price = rs.getInt("Price");
                image = rs.getString("Image");
                categoryID = rs.getInt("CategoryID");
                product = new Products(id, name, description, price, image, categoryID);
                productList.add(product);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return productList;
    }

    // hàm thêm moi 1 san pham
    public void addProduct(Products product) {
        String sql = "INSERT INTO Products (Name, Description, Price, Image, CategoryID) VALUES (?, ?, ?, ?, ?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, product.getName());
            st.setString(2, product.getDescription());
            st.setInt(3, product.getPrice());
            st.setString(4, product.getImage());
            st.setInt(5, product.getCategoryID());
            st.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // hàm sua san pham
    public void editProduct(int productID, Products updatedProduct) {
        String sql = "UPDATE Products SET Name = ?, Description = ?, Price = ?, Image = ?, CategoryID = ? WHERE ID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, updatedProduct.getName());
            st.setString(2, updatedProduct.getDescription());
            st.setInt(3, updatedProduct.getPrice());
            st.setString(4, updatedProduct.getImage());
            st.setInt(5, updatedProduct.getCategoryID());
            st.setInt(6, productID);
            st.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // hàm xóa san pham
    public void deleteProduct(int productID) {
        try {
            // Cập nhật các mặt hàng liên quan trong bảng OrderItems, đặt ProductID thành null
            String sql = "UPDATE OrderItems SET ProductID = null WHERE ProductID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, productID);
            st.executeUpdate();

            // Xóa sản phẩm từ bảng Products
            String deleteProductSql = "DELETE FROM Products WHERE ID = ?";
            PreparedStatement deleteProductPs = connection.prepareStatement(deleteProductSql);
            deleteProductPs.setInt(1, productID);
            deleteProductPs.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ProductsDAO dao = new ProductsDAO();
        List<Products> list = dao.getAllProducts();

        for (Products o : list) {
            System.out.println(o);

        }
    }
}
