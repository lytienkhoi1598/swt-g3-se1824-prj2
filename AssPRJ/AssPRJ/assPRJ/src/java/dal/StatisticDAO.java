package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import model.*;

public class StatisticDAO extends DBContext {

    public int userCount() {
        String sql = "select count(ID) as UserValue from Users";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("UserValue");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int TotalIncome() {
        String sql = "select sum(TotalAmount) as AvgValue from Orders";
        try {
           PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("AvgValue");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public HashMap<String, Integer> IncomeByMonth() {
        HashMap<String, Integer> arr = new HashMap();
        String sql = "select sum(TotalAmount) as total, FORMAT(OrderDate, 'MMM') as date from Orders GROUP BY FORMAT(OrderDate, 'MMM') ORDER BY FORMAT(OrderDate, 'MMM')";
        try {
             PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                arr.put(rs.getString("date"), rs.getInt("total"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arr;
    }

    public int AvgIncome() {
        String sql = "select avg(TotalAmount) as AvgValue from Orders";
        try {
             PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("AvgValue");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public String MinIncome() {
        String sql = "SELECT TOP(1) sum(TotalAmount) as Total, FORMAT(OrderDate, 'MM-yyyy') as Date FROM Orders GROUP BY FORMAT(OrderDate, 'MM-yyyy') ORDER BY Total ASC";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getString("Date") + " (" + rs.getInt("Total") + "VND)";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String MaxIncome() {
        String sql = "SELECT TOP(1) sum(TotalAmount) as Total, FORMAT(OrderDate, 'MM-yyyy') as Date FROM Orders GROUP BY FORMAT(OrderDate, 'MM-yyyy') ORDER BY Total DESC";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getString("Date") + " (" + rs.getInt("Total") + "VND)";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public int productCount() {
        String sql = "select count(ID) as ProductValue from Products";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("ProductValue");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
