package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import model.*;

public class UsersDAO extends DBContext {
    public static void main(String[] args) {
        UsersDAO ud = new UsersDAO();
        List<Users> list = ud.getAllUser();
        for(Users c: list){
            System.out.println(c);
        }
    }

    // hàm dang nhâp
public Users Login(String username, String pass) {
    String sql = "SELECT * FROM Users WHERE username = ? AND password = ?";
    String xUsername, xPass;
    int xId, xRole;
    Users x = null;
    try {
        PreparedStatement st = connection.prepareStatement(sql);
        st.setString(1, username);
        st.setString(2, pass);
        ResultSet rs = st.executeQuery();
        if (rs.next()) {
            xId = rs.getInt("ID");
            xUsername = rs.getString("username");
            xPass = rs.getString("password");
            xRole = rs.getInt("role_id");

            x = new Users(xId, xUsername, xPass, xRole);
        }
    } catch (Exception e) {
        e.printStackTrace();
    }
    return x;
}
    
    public void changePass(int id, String newPass) {
        String sql = "UPDATE Users SET password = ? where ID = ?";
        String xUsername, xPass;
        int xId, xRole;
        Users x = null;
        try {
            PreparedStatement st = connection.prepareStatement(sql);           
            st.setString(1, newPass);
            st.setInt(2, id);
            st.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // hàm tra vê danh sách tat ca nguoi dung
    public List<Users> getAllUser() {
        List<Users> list = new ArrayList<>();
        String sql = "select * from users";
        String xPass, xUsername;
        int xId, xRole;
        Users x = null;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                xId = rs.getInt("ID");
                xUsername = rs.getString("username");
                xPass = rs.getString("password");
                xRole = rs.getInt("role_id");

                x = new Users(xId, xUsername, xPass, xRole);
                list.add(x);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    
    // hàm dang kí
    public void Register(String username, String pass) {
        String sql = "INSERT INTO Users (username, Password, role_id) VALUES ( ?, ?, ?)";

        try {
            PreparedStatement st = connection.prepareStatement(sql);

            st.setString(1, username);
            st.setString(2, pass);
            st.setInt(3, 2);
            ResultSet rs = st.executeQuery();
            st.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
