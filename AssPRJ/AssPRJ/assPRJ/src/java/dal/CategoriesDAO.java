package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.*;

public class CategoriesDAO extends DBContext {

    // lay ra tat ca category
    public List<Categories> getCategories() {

        List<Categories> list = new ArrayList<>();
        String sql = "SELECT * FROM Categories";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Categories s = new Categories(rs.getInt("id"), rs.getString("name"));
                list.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    // lay ra category cua 1 san pham
    public String getCategoryByProductId(int productId) {
        String categoryName = null;
        String sql = "SELECT c.Name FROM Categories c "
                + "INNER JOIN Products p ON c.ID = p.CategoryID "
                + "WHERE p.ID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, productId);
            st.executeQuery();
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                categoryName = rs.getString("Name");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return categoryName;
    }

    // thêm moi 1 category
    public void addCategory(String categoryName) {
        String sql = "INSERT INTO Categories (Name) VALUES (?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, categoryName);
            st.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // cap nhat category
    public void updateCategory(int cateID, String newName) {
        String sql = "UPDATE Categories SET Name = ? WHERE ID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "newName");
            st.setInt(2, cateID);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // xóa category
    public void deleteCategory(int cateID) {
        try {
            // Set foreign key references to null in other tables
            String sql = "UPDATE Products SET CategoryID = NULL WHERE CategoryID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, cateID);
            st.executeUpdate();

            // Delete the category from Categories table
            String sql1 = "DELETE FROM Categories WHERE ID = ?";
            PreparedStatement st1 = connection.prepareStatement(sql1);
            st1.setInt(1, cateID);
            st1.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        CategoriesDAO dao = new CategoriesDAO();
        List<Categories> list = dao.getCategories();
        
        for(Categories c: list){
            System.out.println(c);
        }
    }

}
