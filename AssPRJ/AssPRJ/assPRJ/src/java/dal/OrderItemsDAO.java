package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.*;


public class OrderItemsDAO extends DBContext {

    // lay ra thong tin cua don hàng
    public List<OrderItems> getOdDetailByOdId(int odId) {
        List<OrderItems> List = new ArrayList<>();
        String sql = "select oi.*, p.Name, p.Price from OrderItems oi join Products p\n"
                + "on oi.ProductID = p.ID where oi.OrderID = ?";
        try {
            int productId, quantity, price;
            String name;
            OrderItems detail;
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, odId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                productId = rs.getInt("ProductID");
                name = rs.getString("Name");
                quantity = rs.getInt("Quantity");
                price = rs.getInt("Price");
                detail = new OrderItems(odId, productId, name, quantity, price);
                List.add(detail);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return List;
    }
   

    // thêm 1 orderItems vào order moi
    public void insert(int ProductID, int Quantity) {
        try {
            String sql = "INSERT INTO OrderItems (OrderID, ProductID, Quantity)\n"
                    + "SELECT MAX(id), ?, ?\n"
                    + "FROM Orders;";
            PreparedStatement st = connection.prepareStatement(sql);

            st.setInt(1, ProductID);
            st.setInt(2, Quantity);
            st.executeUpdate();
             st.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    
}
