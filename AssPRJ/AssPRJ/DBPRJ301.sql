USE [master]
GO
/****** Object:  Database [DBPRJ301]    Script Date: 18/05/2024 22:54:05 ******/
CREATE DATABASE [DBPRJ301]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DBPRJ301', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\DBPRJ301.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'DBPRJ301_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\DBPRJ301_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [DBPRJ301] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DBPRJ301].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DBPRJ301] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DBPRJ301] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DBPRJ301] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DBPRJ301] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DBPRJ301] SET ARITHABORT OFF 
GO
ALTER DATABASE [DBPRJ301] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DBPRJ301] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DBPRJ301] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DBPRJ301] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DBPRJ301] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DBPRJ301] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DBPRJ301] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DBPRJ301] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DBPRJ301] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DBPRJ301] SET  ENABLE_BROKER 
GO
ALTER DATABASE [DBPRJ301] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DBPRJ301] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DBPRJ301] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DBPRJ301] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DBPRJ301] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DBPRJ301] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DBPRJ301] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DBPRJ301] SET RECOVERY FULL 
GO
ALTER DATABASE [DBPRJ301] SET  MULTI_USER 
GO
ALTER DATABASE [DBPRJ301] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DBPRJ301] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DBPRJ301] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DBPRJ301] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [DBPRJ301] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [DBPRJ301] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'DBPRJ301', N'ON'
GO
ALTER DATABASE [DBPRJ301] SET QUERY_STORE = ON
GO
ALTER DATABASE [DBPRJ301] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [DBPRJ301]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 18/05/2024 22:54:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderItems]    Script Date: 18/05/2024 22:54:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderItems](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OrderID] [int] NULL,
	[ProductID] [int] NULL,
	[Quantity] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 18/05/2024 22:54:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[Name] [nvarchar](255) NULL,
	[phonenumber] [nvarchar](20) NULL,
	[Address] [nvarchar](255) NULL,
	[OrderDate] [date] NULL,
	[TotalAmount] [int] NULL,
	[StatusID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderStatus]    Script Date: 18/05/2024 22:54:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStatus](
	[orderstatus_id] [int] IDENTITY(1,1) NOT NULL,
	[status_name] [nvarchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[orderstatus_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 18/05/2024 22:54:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[Price] [int] NULL,
	[Image] [nvarchar](max) NULL,
	[CategoryID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[role]    Script Date: 18/05/2024 22:54:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[role](
	[role_id] [int] IDENTITY(1,1) NOT NULL,
	[role_name] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 18/05/2024 22:54:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](255) NULL,
	[Password] [nvarchar](255) NULL,
	[role_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([ID], [Name]) VALUES (1, N'Rau củ')
INSERT [dbo].[Categories] ([ID], [Name]) VALUES (2, N'Thực phẩm sẵn')
INSERT [dbo].[Categories] ([ID], [Name]) VALUES (3, N'Đồ ăn nhanh')
INSERT [dbo].[Categories] ([ID], [Name]) VALUES (4, N'Đồ uống')
SET IDENTITY_INSERT [dbo].[Categories] OFF
GO
SET IDENTITY_INSERT [dbo].[OrderItems] ON 

INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (1, 1, 3, 1)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (2, 1, 4, 1)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (3, 1, 2, 1)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (4, 2, 15, 10)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (5, 2, 14, 1)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (6, 3, 13, 2)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (7, 3, 19, 2)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (8, 3, 29, 2)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (9, 6, 30, 3)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (10, 6, 30, 3)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (11, 7, 14, 1)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (12, 7, 14, 1)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (13, 7, 28, 3)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (14, 7, 28, 3)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (15, 8, 1, 3)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (16, 8, 1, 3)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (17, 8, 30, 1)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (18, 8, 30, 1)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (19, 9, 17, 1)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (20, 9, 17, 1)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (21, 9, 23, 1)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (22, 9, 23, 1)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (23, 10, 29, 1)
INSERT [dbo].[OrderItems] ([ID], [OrderID], [ProductID], [Quantity]) VALUES (24, 10, 29, 1)
SET IDENTITY_INSERT [dbo].[OrderItems] OFF
GO
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([ID], [UserID], [Name], [phonenumber], [Address], [OrderDate], [TotalAmount], [StatusID]) VALUES (1, 2, N'Trung Tuyến', N'0375305666', N'gdg', CAST(N'2024-02-29' AS Date), 43000, 3)
INSERT [dbo].[Orders] ([ID], [UserID], [Name], [phonenumber], [Address], [OrderDate], [TotalAmount], [StatusID]) VALUES (2, 3, N'MUA', N'0967215999', N'KHU2', CAST(N'2024-03-07' AS Date), 825000, 3)
INSERT [dbo].[Orders] ([ID], [UserID], [Name], [phonenumber], [Address], [OrderDate], [TotalAmount], [StatusID]) VALUES (3, 4, N'Trung', N'0999123456', N'FPT', CAST(N'2024-03-07' AS Date), 230000, 3)
INSERT [dbo].[Orders] ([ID], [UserID], [Name], [phonenumber], [Address], [OrderDate], [TotalAmount], [StatusID]) VALUES (6, 5, N'mua', N'0999123456', N'VINHPHUC', CAST(N'2024-03-14' AS Date), 105000, 3)
INSERT [dbo].[Orders] ([ID], [UserID], [Name], [phonenumber], [Address], [OrderDate], [TotalAmount], [StatusID]) VALUES (7, 2, N'Hihi', N'19001500', N'VINHPHUC', CAST(N'2024-03-14' AS Date), 100000, 3)
INSERT [dbo].[Orders] ([ID], [UserID], [Name], [phonenumber], [Address], [OrderDate], [TotalAmount], [StatusID]) VALUES (8, 7, N'Trung Tuyến', N'0375305666', N'gdg', CAST(N'2024-03-15' AS Date), 101000, 3)
INSERT [dbo].[Orders] ([ID], [UserID], [Name], [phonenumber], [Address], [OrderDate], [TotalAmount], [StatusID]) VALUES (9, 7, N'mua', N'0375305666', N'vietnam', CAST(N'2024-03-15' AS Date), 55000, 3)
INSERT [dbo].[Orders] ([ID], [UserID], [Name], [phonenumber], [Address], [OrderDate], [TotalAmount], [StatusID]) VALUES (10, 2, N'Sqdwad', N'099992', N'ưdwadwa', CAST(N'2024-05-13' AS Date), 20000, 1)
SET IDENTITY_INSERT [dbo].[Orders] OFF
GO
SET IDENTITY_INSERT [dbo].[OrderStatus] ON 

INSERT [dbo].[OrderStatus] ([orderstatus_id], [status_name]) VALUES (1, N'wait')
INSERT [dbo].[OrderStatus] ([orderstatus_id], [status_name]) VALUES (2, N'process')
INSERT [dbo].[OrderStatus] ([orderstatus_id], [status_name]) VALUES (3, N'done')
SET IDENTITY_INSERT [dbo].[OrderStatus] OFF
GO
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (1, N'Mướp Đắng VietGAP', N'Bảo quản nhiệt độ mát 5 - 10 độ
Lưu ý:Không sử dụng khi sản phẩm có dấu hiệu hư hỏng', 22000, N'img/product/muopdang.jpeg', 1)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (2, N'Sấu Bào Vỏ', N'Sử dụng khi còn tươi
Tủ đông
Lưu ý:Không sử dụng khi sản phẩm có dấu hiệu hư hỏng', 10000, N'img/product/saubaovo.png', 1)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (3, N'Hành Tây VietGAP', N'Dùng ngay khi nhận hàng
Bảo quản nhiệt độ mát 5 - 10 độ
Lưu ý:Không sử dụng khi sản phẩm có dấu hiệu hư hỏng', 16000, N'img/product/hanhtay.jpeg', 1)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (4, N'Nấm Kim Châm Hữu Cơ', N'Dùng ngay khi nhận hàng
Bảo quản nhiệt độ mát 5 - 10 độ
Lưu ý:Không sử dụng khi sản phẩm có dấu hiệu hư hỏng', 17000, N'img/product/namkimcham.jpeg', 1)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (5, N'Rau Muống Lá VietGAP', N'Sử dụng khi còn tươi
Bảo quản nhiệt độ mát 5 - 10 độ
Lưu ý:Không sử dụng khi sản phẩm có dấu hiệu hư hỏng', 14000, N'img/product/raumuong.jpeg', 1)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (6, N'Củ Cải Trắng', N'Sử dụng khi còn tươi
Bảo quản nhiệt độ mát 5 - 10 độ
Lưu ý:Không sử dụng khi sản phẩm có dấu hiệu hư hỏng', 8000, N'img/product/cucaitrang.jpeg', 1)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (7, N'Rau Dền VietGAP', N'Sử dụng khi còn tươi
Bảo quản nhiệt độ mát 5 - 10 độ
Lưu ý:Không sử dụng khi sản phẩm có dấu hiệu hư hỏng', 13000, N'img/product/rauden.jpeg', 1)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (8, N'Ớt Chuông Đỏ', N'Bảo quản nhiệt độ mát 5 - 10 độ
Lưu ý:Không sử dụng khi sản phẩm có dấu hiệu hư hỏ  ng', 30000, N'img/product/otchuongdo.jpeg', 2)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (9, N'Bắp Cải VietGAP', N'Bảo quản nhiệt độ mát 5 - 10 độ
Lưu ý:Không sử dụng khi sản phẩm có dấu hiệu hư hỏng', 24000, N'img/product/bapcai.jpeg', 1)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (10, N'Canh Chua Cá Quả', N'Bước 1
Rửa sạch cá quả đã sơ chế, để ráo nước. Rửa sạch nguyên liệu nấu canh, để ráo nước. Cà chua cắt múi cau. Dọc mùng tước vỏ, cắt miếng vừa ăn, bóp muối. Sau đó chần sơ dọc mùng qua với nước sôi, vắt sạch để dọc mùng không bị ngứa và mặn. Mùi tàu, rau ngổ cắt nhỏ.
Bước 2
Cho 1 muỗng canh dầu ăn vào nồi, đợi dầu nóng cho hành tím băm, đầu hành băm vào phi thơm, rồi rót 750ml nước lọc vào nồi, thả cá vào nấu khoảng 10 phút cho chín, rồi nhẹ nhàng vớt ra. Thả dứa vào nấu 2 phút, sau đó thêm tiếp cà chua.
Bước 3
Khi nước sôi lăn tăn, cho từ từ gói gia vị khô và nêm thêm nước mắm, nêm nếm cho vừa ăn. Thả cá đã chín vào cùng giá đỗ, dọc mùng nấu thêm 1 - 2 phút cho canh sôi bùng. Rắc mùi tàu, rau ngổ lên mặt rồi tắt bếp.
* Bí quyết: Thêm nước mắm làm dậy mùi thơm và canh thêm đậm đà
Bước 4
Cho canh ra tô, rắc thêm tỏi phi để tăng thêm hương vị.', 72000, N'img/product/canhchuacaqua.jpeg', 2)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (11, N'Sườn Non Heo Kho Trứng Cút', N'Bước 1:
Rửa sạch các nguyên liệu đã sơ chế, để ráo nước. Hành lá cắt nhỏ. Luộc trứng cút chín rồi bóc vỏ để riêng. Ướp sườn với gói gia vị hoàn chỉnh kho Việt trong 15 phút.
Bước 2:
Đặt nồi lên bếp, cho 2 muỗng canh dầu ăn vào nồi, đợi dầu nóng, phi thơm hành tím băm, tỏi băm. Sau đó, cho hỗn hợp sườn ướp vào xào cho thịt săn lại.
Bước 3:
Tiếp đến, rót 100 ml nước lọc, nước dừa và trứng cút vào nấu khoảng 15 - 20 phút đến khi nước có độ sệt. Thêm hành lá vào nồi, nêm nếm cho vừa ăn rồi tắt bếp.
Bước 4:
Bày món ăn ra dĩa, rắc tiêu xay lên mặt, trang trí tùy thích và thưởng thức. Ngon hơn khi ăn nóng cùng cơm trắng.
Ghi chú:
- Gói gia vị nên đổ từ từ (không đổ hết) để nêm nếm cho vừa ăn.
- Có thể thay đổi định lượng nước và gia vị để phù hợp với khẩu phần và khẩu vị.', 64000, N'img/product/suonnonheokho.jpeg', 2)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (12, N'Ba Chỉ Heo Rang Cháy Cạnh', N'Bước 1:
Rửa sạch các nguyên liệu đã qua sơ chế để ráo nước. Hành lá cắt nhỏ.
Bước 2:
Đặt chảo lên bếp cho 1 muỗng canh dầu ăn vào chảo đợi dầu ăn nóng cho thịt ba rọi vào rang trên lửa lớn cho thịt vàng cháy cạnh.
Bước 3:
Tiếp đến cho gói sốt gia vị vào đảo lên cho gia vị thấm đều vào thịt, nấu trên lửa nhỏ trong 5 phút. Nêm lại cho vừa ăn rồi tắt bếp.
Bước 4:
Bày món ăn ra đĩa, hành lá, ớt chỉ thiên cắt nhỏ cùng một ít tiêu xay cho lên mặt. Ngon khi thưởng thức nóng cùng cơm trắng.', 52000, N'img/product/bachiheorang.jpeg', 2)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (13, N'Burger Gà Classic', N'Burger gà tươi ngon với sốt mayonnaise và rau sống.', 35000, N'img/product/burgergaclassic.jfif', 3)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (14, N'Bánh Mì Phô Mai BBQ', N'Bánh mì mềm xốp, nhân phô mai và thịt BBQ nướng chín.', 25000, N'img/product/banhmifomai.jfif', 3)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (15, N'Pizza Pepperoni', N'Pizza thơm ngon với phô mai, xốt cà chua và lớp pepperoni.', 80000, N'img/product/pizzapep.jfif', 3)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (16, N'Bánh Mì Bơ Ớt', N'Bánh mì giòn tan, phết bơ và ớt, hấp dẫn từng miếng.', 20000, N'img/product/banhmiboot.jfif', 3)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (17, N'Hotdog Chảo', N'Hotdog nóng hổi với xốt ớt, hành tây và bơ.', 30000, N'img/product/hotdogchao.jfif', 3)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (18, N'Gỏi Cuốn Tôm', N'Gỏi cuốn tươi mát, nhân tôm, rau sống và bún.', 50000, N'img/product/goicuontom.jpg', 3)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (19, N'Sushi Cuộn Phô Mai', N'Sushi cuộn với lớp phô mai béo ngậy và tôm.', 60000, N'img/product/sushicuon.png', 3)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (20, N'Salad Gà Cesar', N'Salad rau sống, gà nướng và sốt Cesar thơm ngon.', 45000, N'img/product/saladga.jfif', 3)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (21, N'Bánh Pizza Que', N'Pizza nhỏ xinh, ăn vặt với que tiện lợi.', 35000, N'img/product/pizzaque.jfif', 3)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (22, N'Cà Phê Sữa Đá', N'Cà phê đậm đà, ngon miệng, pha cùng sữa đặc có đá.', 20000, N'img/product/cafesuada.jfif', 4)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (23, N'Trà Sữa Thái Xanh', N'Trà xanh thơm ngon, pha chế kèm sữa tươi và bọt.', 25000, N'img/product/trasuathaixanh.jfif', 4)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (24, N'Nước Mía Lá Dứa', N'Nước mía tươi ngon, thêm lá dứa cho hương vị đặc biệt.', 20000, N'img/product/nuocmialadua.jpeg', 4)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (25, N'Sinh Tố Dừa', N'Sự kết hợp hoàn hảo giữa dừa tươi và sữa tươi.', 35000, N'img/product/sinhtodua.jpg', 4)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (26, N'Bia Hơi', N'Bia tươi ngon, rất phổ biến trong các quán ăn và quán nhậu.', 20000, N'img/product/biahoihanoi.jfif', 4)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (27, N'Cốt Dừa', N'Đồ uống lạ miệng, với cốt dừa, đá và sữa đặc.', 18000, N'img/product/nuoccotdua.jfif', 4)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (28, N'Sinh Tố Chanh Xanh', N'Sinh tố chua ngọt, bổ dưỡng từ chanh xanh và đường.', 25000, N'img/product/sinhtotranh.jfif', 4)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (29, N'Nước Ép Cà Rốt', N'Nước ép cà rốt tươi ngon, giàu vitamin A.', 20000, N'img/product/nuocepcarot.jfif', 4)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (30, N'Sữa Chua Nếp Cẩm', N'Sữa chua truyền thống kết hợp với nếp cẩm thơm ngon.', 35000, N'img/product/suachuanepcam.jfif', 4)
INSERT [dbo].[Products] ([ID], [Name], [Description], [Price], [Image], [CategoryID]) VALUES (31, N'Mùi Ta VietGAP', N'Dùng ngay khi nhận hàng
Bảo quản nhiệt độ mát 5 - 10 độ
Lưu ý:Không sử dụng khi sản phẩm có dấu hiệu hư hỏng', 7000, N'img/product/muita.jpeg', 1)
SET IDENTITY_INSERT [dbo].[Products] OFF
GO
SET IDENTITY_INSERT [dbo].[role] ON 

INSERT [dbo].[role] ([role_id], [role_name]) VALUES (1, N'Admin')
INSERT [dbo].[role] ([role_id], [role_name]) VALUES (2, N'Customer')
SET IDENTITY_INSERT [dbo].[role] OFF
GO
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([ID], [username], [Password], [role_id]) VALUES (1, N'admin', N'123', 1)
INSERT [dbo].[Users] ([ID], [username], [Password], [role_id]) VALUES (2, N'user', N'123', 2)
INSERT [dbo].[Users] ([ID], [username], [Password], [role_id]) VALUES (3, N'Tuyen', N'123', 2)
INSERT [dbo].[Users] ([ID], [username], [Password], [role_id]) VALUES (4, N'user2', N'12345', 2)
INSERT [dbo].[Users] ([ID], [username], [Password], [role_id]) VALUES (5, N'mua', N'123', 2)
INSERT [dbo].[Users] ([ID], [username], [Password], [role_id]) VALUES (6, N'user5', N'123', 2)
INSERT [dbo].[Users] ([ID], [username], [Password], [role_id]) VALUES (7, N'test', N'12345', 2)
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
ALTER TABLE [dbo].[Orders] ADD  DEFAULT ((0)) FOR [TotalAmount]
GO
ALTER TABLE [dbo].[OrderItems]  WITH CHECK ADD FOREIGN KEY([OrderID])
REFERENCES [dbo].[Orders] ([ID])
GO
ALTER TABLE [dbo].[OrderItems]  WITH CHECK ADD FOREIGN KEY([ProductID])
REFERENCES [dbo].[Products] ([ID])
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([StatusID])
REFERENCES [dbo].[OrderStatus] ([orderstatus_id])
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([ID])
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Categories] ([ID])
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD FOREIGN KEY([role_id])
REFERENCES [dbo].[role] ([role_id])
GO
USE [master]
GO
ALTER DATABASE [DBPRJ301] SET  READ_WRITE 
GO
